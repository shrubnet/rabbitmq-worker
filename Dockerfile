FROM php:7.4-cli-alpine

RUN docker-php-ext-install sockets

COPY --from=composer /usr/bin/composer /usr/bin/composer

WORKDIR /app

ENTRYPOINT composer install && php ./worker.php
