<?php
require_once __DIR__ . '/vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$connection = new AMQPStreamConnection(
    $_ENV[ 'RABBITMQ_HOST' ],
    $_ENV[ 'RABBITMQ_PORT' ],
    $_ENV[ 'RABBITMQ_USER' ],
    $_ENV[ 'RABBITMQ_PASSWORD' ]
);
$channel = $connection->channel();

$channel->queue_declare( $_ENV[ 'RABBITMQ_QUEUE' ], false, false, false, false );

echo " [*] Waiting for messages. To exit press CTRL+C\n";

$callback = function( AMQPMessage $msg ) {
    var_dump( $msg->body );
};

$channel->basic_consume( $_ENV[ 'RABBITMQ_QUEUE' ], '', false, true, false, false, $callback );

while( $channel->is_consuming() ) {
    $channel->wait();
}
